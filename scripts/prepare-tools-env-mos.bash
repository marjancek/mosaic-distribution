#!/dev/null

_distribution_mos_dependencies=(
	# mOSAIC custom packages
		mosaic-utils-0.0.2
		mosaic-erlang-r15b01
		mosaic-nodejs-0.8.4
		mosaic-sun-jdk-7u1
	# Slitaz tools packages
		findutils
		grep
		sed
		gawk
		tar
		zip
		wget
		curl
	# Slitaz language packages
		python
		perl
		gcc
	# Slitaz development packages
		git
		binutils
		libtool
		autoconf
		automake
		pkg-config
		make
	# Slitaz library packages
		jansson
		libxml2-dev
		util-linux-ng-uuid-dev
)
